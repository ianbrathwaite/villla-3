const express = require('express')
const compression = require('compression')
const logger = require('morgan')
const bodyParser = require('body-parser')
const errorHandler = require('errorhandler')
const path = require('path')
const mailer = require('nodemailer')

const app = express()
const transporter = mailer.createTransport(process.env.NODEMAILER)

app.set('port', process.env.PORT || 3000)
app.use(compression())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }))
app.get('/', (req, res) => res.sendFile(path.join(__dirname + '/public/index.html')))
app.get('/cottage', (req, res) => res.sendFile(path.join(__dirname + '/public/cottage.html')))
app.post('/contact', (req, res) => {
  const mail = {
    from: '"Villa des Anges" <villadesanges@gmail.com>',
    to: 'connor.brathwaite@gmail.com',
    subject: 'Villa des Anges Inquery - ' + req.body.name,
    text: req.body.message + ' - from: ' + req.body.email
  }

  transporter.sendMail(mail, (err, info) => {
    if (err) {
      return res.status(500).send({ error: 'something blew up' })
    }

    res.status(200)
  })
})
app.get('*', (req, res) => res.redirect('/'))
app.use(errorHandler())
app.listen(app.get('port'), () => {
  console.log('Maginc happening on port %d in %s mode', app.get('port'), app.get('env'))
})
